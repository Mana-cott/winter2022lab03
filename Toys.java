public class Toys{
	
	//fields
	public String theme;
	//theme of the toy line
	//ex. robots, pirates, castles
	public String material;
	//name of the material used
	//ex. plastic, fabric, foam, slime
	public int ageLevel;
	//recommended age level
	
	//instance method
	public void toyAction(){
		if(this.theme.equals("robots")){
			System.out.println("BEEP");
		}
		else if(this.theme.equals("pirates")){
			System.out.println("AARGH!");
		}
		else if(this.theme.equals("castles")){
			System.out.println("AAAAA! for the castle!");
		}
		else
		{
			System.out.println("KABLAM!");
		}
			
	}
	
}