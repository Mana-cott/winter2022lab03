import java.util.Scanner;
public class Shop{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		Toys[] toy = new Toys[4];
		for(int i = 0; i < toy.length; i++){
			
			toy[i] = new Toys();
			
			//fancy stuff to make it more interesting for user
			String ordinal = " ";
			if(i == 0){
				ordinal = "first";
			}
			else if(i == 1){
				ordinal = "second";
			}
			else if(i == 2){
				ordinal = "third";
			}
			else if(i == 3){
				ordinal = "fourth";
			}
			
			//put this here to be easier for user to see what question they're on
			int questionNumber = i + 1;
			System.out.println("~~ " + questionNumber + ".");
			System.out.println("Please insert the theme for your " + ordinal + " toy!");
			System.out.println("Example: robots, pirates, castles, etc...");
			toy[i].theme = scan.next();
			System.out.println("Please insert the recommended age level for your " + ordinal + " toy.");
			toy[i].ageLevel = scan.nextInt();
			System.out.println("What material is the " + ordinal + " toy made from?");
			System.out.println("Example: plastic, fabric, foam, slime, etc...");
			toy[i].material = scan.next();
			System.out.println(" ");
		}
		System.out.println("Now the last three fields entered will be printed back.");
		System.out.println(toy[3].theme);
		System.out.println(toy[3].ageLevel);
		System.out.println(toy[3].material);
		//sound effect action
		toy[3].toyAction();
		
	}	
}